<?php

include_once 'debug.php';
$records = array(
    array(
        'id' => 2000,
        'first_name' => 'Sumon',
        'last_name' => 'Mahmud',
    ),
    array(
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ),
    array(
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ),
    array(
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    )
);

debug($records);

$first_names = array_column($records, 'first_name');
debug($first_names);

$last_name = array_column($records, 'last_name');
debug($last_name);


$id = array_column($records, 'id');
debug($id);

for ($i = 0; $i < count($id); $i++) {
    if ($id[$i] == 2000) {
        echo "This is sumon mahmud ID is : " . $id[$i] . "<br/>";
    } else {
        echo "This is other's name ID is : " . $id[$i] . "<br/>";
    }
}
