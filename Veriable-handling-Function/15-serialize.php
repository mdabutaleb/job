<?php

echo "<pre>";
$a = array('Sumon', 'Mukta', 'Mahmud');
echo "print without serialize" . "<br/>";
print_r($a);
echo "<pre>";
echo "print after serialize" . "<br/>";
$serialize = serialize($a);
print_r($serialize);
echo "<pre>";
echo "<br/>" . "print after unserialize";
$unserialize = unserialize($serialize);
print_r($unserialize);
