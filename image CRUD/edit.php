<!DOCTYPE html>
<?php
include('lib/image.php');
include('lib/debug.php');
$id = $_GET['id'];

if (isset($id)) {
    $obj = new image();
    $image = $obj->findone($id);
} else {
    header('location:index.php');
}

?>
<html>
<body>
<a href="index.php">SHOW</a>
<a href="create.php">ADD</a>

<form action="update.php?id=<?php echo $id ?>" method="post" enctype="multipart/form-data">
    <h1>FILE/IMAGE UPDATE</h1>

    <p><input type="hidden" name="id" value="<?php echo $id ?>"></p> <!-- hidden field -->
    <hr/>
    <p><input type="file" name="image_up"></p>

    <img src="<?php echo $image['img_path'] ?>" alt="image" height="100" width="100">

    <p><input type="submit" name="btn" value="update"></p>
</form>
</body>
</html>