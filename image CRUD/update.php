<?php

include('lib/image.php');
if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {

    $id = $_GET['id'];

    if (isset($id)) {
        $filetmp = $_FILES["image_up"]["tmp_name"];
        $filename = $_FILES["image_up"]["name"];
        $filetype = $_FILES["image_up"]["type"];
        $filepath = "files/" . $filename;
        move_uploaded_file($filetmp, $filepath);
        $obj = new image();
        $obj->update($filename, $filetype, $filepath, $id);
    } else {
        header('location:index.php');
    }


} else {
    echo "somthing going wrong";
}