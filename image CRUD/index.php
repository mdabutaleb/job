<!DOCTYPE html>
<html>
<body>
<?php
include('lib/image.php');
include('lib/debug.php');
$obj = new image();
$images = $obj->findall();
//debug($images);
?>
<a href="create.php">ADD</a>
<table border="1" align="center" width="90%"
       cellpadding="10px">
    <tr>
        <th>Id</th>
        <th>Image</th>
        <th>Image Name</th>
        <th>Image Type</th>
        <th>Action</th>
    </tr>
    <?php foreach ($images as $image) {

    ?>
    <tr>
        <td><?php echo $image['id'];?></td>
        <td><img src="<?php echo $image['img_path'];?>" height="100px" width="130px"></td>
        <td><?php echo $image['img_name']?></td>
        <td><?php echo $image['img_type']?></td>
        <td>
            <a href="view.php?id=<?php echo $image['id'] ?>">view</a>
            <a href="edit.php?id=<?php echo $image['id'] ?>">edit</a>
            <a href="delete.php?id=<?php echo $image['id'] ?>">delete</a>
        </td>
    </tr>
    <?php } ?>
</table>
</body>
</html>